void setup()
{
    Serial.begin(115200);
}

void loop()
{
    float sensorVoltage;
    float sensorValue;

    sensorValue = analogRead(A0);
    sensorVoltage = sensorValue / 1024 * 3.3; //10bit分辨率，3.3V电源

    Serial.print("sensor voltage = ");
    Serial.print(sensorVoltage);
    Serial.println(" V");
    delay(1000);
}
