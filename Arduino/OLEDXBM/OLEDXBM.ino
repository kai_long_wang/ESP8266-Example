//在OLED屏幕上显示位图，使用U8G2库

#include <U8g2lib.h>
#include "hqyj.h"

//使用SSD1306控制器
//128x64像素
//无名山寨屏
//全屏幕缓冲
//软件I2C控制器（水平方向，SCL=D5，SDA=D6）
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, D5, D6);

void setup(void) {
    u8g2.begin();//初始化
}

void loop(void) {
    //80MHz时每秒最高可以显示5帧，160MHz每秒10帧
    u8g2.clearBuffer();//清空缓冲区
    u8g2.drawXBM(0, 0, hqyj_width, hqyj_height, (uint8_t*)hqyj_bits);
    u8g2.sendBuffer();//显示缓冲区内容
}
