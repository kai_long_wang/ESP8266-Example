//在OLED屏幕上显示中文字符，使用U8G2库

#include <U8g2lib.h>
#include <DHT.h>

DHT sensor(D1, DHT11);

//使用SSD1306控制器
//128x64像素
//无名山寨屏
//全屏幕缓冲
//软件I2C控制器（水平方向，SCL=D5，SDA=D6）
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, D5, D6);

void setup(void) {
  u8g2.begin();//初始化
  u8g2.setFont(u8g2_font_wqy16_t_gb2312);//使用自带的16像素文泉驿字体，字符集GB2312
  sensor.begin();
}

void loop(void) {
  float temp = sensor.readTemperature();
  float humi = sensor.readHumidity();
  String s1 = "温度：" + String(temp);
  String s2 = "湿度：" + String(humi);
  u8g2.clearBuffer();//清空缓冲区
  u8g2.drawUTF8(0, 16, s1.c_str());
  u8g2.drawUTF8(0, 32, s2.c_str());
  u8g2.sendBuffer();//显示缓冲区内容
  delay(1000);
}
