#include <ESP8266WiFi.h>

void setup() {
    Serial.begin(115200);
    WiFi.mode(WIFI_STA);
    WiFi.begin("LIUYU", "12345678");
    while (!WiFi.isConnected())
    {
        delay(500);
        Serial.print('.');
    }
    Serial.println(WiFi.localIP());
    configTime(8 * 3600, 0, "pool.ntp.org");
}

void loop() {
    time_t  now = time(nullptr);
    Serial.print(ctime(&now));
    delay(1000);
}
